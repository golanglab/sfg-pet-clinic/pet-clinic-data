package model

type person struct {
	FirstName string
	LastName  string
}

func NewPerson(firstName, lastName string) *person {
	return &person{firstName, lastName}
}
