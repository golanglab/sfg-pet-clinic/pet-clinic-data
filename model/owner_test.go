package model

import (
	"fmt"
	"testing"
)

func TestOwner(t *testing.T) {
	got := &Owner{}
	got.FirstName = "Vasya"
	got.LastName = "Pupkin"

	fmt.Printf("%#v %v", &got, got)

	if got.FirstName != "Vasya" {
		t.Errorf("Owner first name = %s ; want Vasya", got.FirstName)
	}

	got.person = person{"Petya", "Zaikin"}
	fmt.Printf("%#v %v", &got, got)
	got = &Owner{person{"Borya", "Sichkin"}}
	fmt.Printf("%#v %v", &got, got)
}
