package model

import "testing"

func TestPerson(t *testing.T) {
	got := NewPerson("Vasya", "Pypkin")

	if got.FirstName != "Vasya" {
		t.Errorf("Person First Name = %s; want Vasya", got.FirstName)
	}

	if got.LastName != "Pypkin" {
		t.Errorf("Person Last Name = %s; want Pypkin", got.LastName)
	}
}
